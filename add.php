<?php
	require_once "functions/menu.php";
	head();
	menu();
	echo '<h2>Add a book</h2>';
	inputs(1);
	$book="";
	$cont=0;
	do{
		if(isset($_POST["ok"])){
			if(0==is_int($_POST["isbn"])&&13==strlen($_POST["isbn"])){
				$book .= $_POST["isbn"].";";
				$cont++;
			}else{echo "ISBN not valid.";}

			if(1<strlen($_POST["title"])){
				$book .= $_POST["title"].";";
				$cont++;
			}else{echo "Title required.";}

			if(1<strlen($_POST["author"])){
				$book .= $_POST["author"].";";
				$cont++;
			}else{echo "Author not valid.";}

			if(isset($_POST["gender"])){
				$book .= $_POST["gender"].";";
				$cont++;
			}else{echo "Gender required.";}

			if(0==is_int($_POST["year"]) && 4 == strlen($_POST["year"])){
				$book .= $_POST["year"].";";
				$cont++;
			}else{echo "Year not valid.";}

			if($cont==5){
				if(false==is_readable("txt/biblioteca.txt")){
					echo "ERROR, Database not exist.";
				}
				else{
					if(false==is_readable("txt/biblioteca.txt")){
						echo "ERROR, Database not exist.";
					}
					else{
						$file=fopen("txt/biblioteca.txt","a+");
						fputs($file, $book.PHP_EOL);
						fclose($file);
						echo "<br/>Book is added";
					}
				}
			}
		}
	}while($cont==-1);
	footer();
?>
