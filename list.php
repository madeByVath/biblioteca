<?php
	require_once "functions/menu.php";
	head();
	menu();
	echo '<h2>List of books</h2>';
	if(false==is_readable("txt/biblioteca.txt")){
		echo "ERROR, Database not exist.";
	}
	else{
		$file2 = fopen("txt/biblioteca.txt", "r");
		$table="<table>";
		$table.="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Gender</th><th>Year</th></tr>";
		$linia = fgets($file2);
		while(!feof($file2)){
			/*print_r (explode(";",$linia));*/
			$porciones = explode(";", $linia);
			$table.="<tr>";

			for($j=0;$j<5;$j++){
				$table.="<td>";
				$table.=$porciones[$j];
				$table.="</td>";
			}
			$table.="</tr>";
		$linia = fgets($file2);
		}//END While
		$table.="</table>";
		echo $table;
		fclose($file2);
	}
	footer();
?>
