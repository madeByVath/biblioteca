<?php
  require_once "functions/menu.php";
  head();
  menu();
  echo '<h2>Delete a book</h2>';
  //print all books with delete checkbox
  if(false==is_readable("txt/biblioteca.txt")){
    echo "ERROR, Database not exist.";
  }
  else{
    $file2 = fopen("txt/biblioteca.txt", "r");
    echo '<form method="post" action="">';
    $table="<table>";
    $table.="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Gender</th><th>Year</th><th>Delete</th></tr>";
    $linia = fgets($file2);
    $cont=0;
    while(!feof($file2)){
      /*print_r (explode(";",$linia));*/
      $porciones = explode(";", $linia);
      $table.="<tr>";

      for($j=0;$j<5;$j++){
        $table.="<td>".$porciones[$j]."</td>";
      }

      $table.='<td><input type="checkbox" name="del[]" value="'.$cont.'" </td>';
      $cont++;
      $table.="</tr>";
    $linia = fgets($file2);
    }//END While
    $table.="</table>";
    echo $table;
    echo '<input type="submit" name="delete" value="Delete" /></form>';
    fclose($file2);
  }
  //take results and write in a temp file
  if(isset($_POST["delete"])){
    $file2 = fopen("txt/biblioteca.txt", "r");
    $tempFile = fopen("txt/temp.txt", "w");

    $cont2=0;
    $deleteSelection = array();
    if(isset($_POST['del'])){
      $deleteSelection = $_POST['del'];

      print_r ($deleteSelection);

      $linia = fgets($file2);
      while(!feof($file2)){
        $flag=0;
        for($i=0;$i<count($deleteSelection);$i++){
          if($cont2==$deleteSelection[$i]){
            $flag=1;
          }
        }
        if($flag==0){
          fputs($tempFile, $linia);
        }
        $cont2++;
        $linia = fgets($file2);
      }//END While
      unlink("txt/biblioteca.txt");
      rename("txt/temp.txt","txt/biblioteca.txt");
      header('location: delete.php');
    }
  }
  footer();
?>
