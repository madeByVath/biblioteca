<?php
	require_once "functions/menu.php";
	head();
	menu();
	echo '<h2>Search a book</h2>';
	inputs(2);
	if(false==is_readable("txt/biblioteca.txt")){
		echo "ERROR, Database not exist.";
	}
	else{
		$file2 = fopen("txt/biblioteca.txt", "r");
		$table="<table>";
		$table.="<tr><th>ISBN</th><th>Title</th><th>Author</th><th>Gender</th><th>Year</th></tr>";
		$linia = fgets($file2);
		while(!feof($file2)){
			/*print_r (explode(";",$linia));*/
			$porciones = explode(";", $linia);
			$contInputs=0;
			$contCorrects=0;
			if(isset($_POST["ok"])){
				if(1<strlen($_POST["isbn"])){
					$contInputs++;
					if(0==strcmp($_POST["isbn"], $porciones[0])){
						$contCorrects++;
					}
				}
				if(1<strlen($_POST["title"])){
					$contInputs++;
					if(0==strcmp($_POST["title"], $porciones[1])){
						$contCorrects++;
					}
				}
				if(1<strlen($_POST["author"])){
					$contInputs++;
					if(0==strcmp($_POST["author"], $porciones[2])){
						$contCorrects++;
					}
				}
				if(1<strlen($_POST["gender"])){
					$contInputs++;
					if(0==strcmp($_POST["gender"], $porciones[3])){
						$contCorrects++;
					}
				}
				if(1<strlen($_POST["year"])){
					$contInputs++;
					if(($_POST["year"] == $porciones[4])){
						$contCorrects++;
					}
				}
			}

		if($contInputs==$contCorrects){
			$table.="<tr>";
			for($j=0;$j<5;$j++){
				$table.="<td>";
				$table.=$porciones[$j];
				$table.="</td>";
			}
			$table.="</tr>";
		}

		$linia = fgets($file2);
		}//END While
		$table.="</table>";
		echo $table;
		fclose($file2);
	}
	footer();
?>
